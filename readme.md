# My honors thesis on playing QWOP with Deep Q Learning

Submitted under the supervision of Maria Gini and James Parker to the University Honors Program at the University of Minnesota-Twin Cities in partial fulfullment of the requirements for the degree of Bachelor of Sciences *cum laude* in Computer Science.

[PDF](https://github.com/hungweiwu/qwop-thesis/blob/master/thesis.pdf)
